import * as React from "react"

const Menu = (props) => (
  <svg
    width={0}
    height={0}
    xmlns="http://www.w3.org/2000/svg"
    className="fill-white"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
    strokeWidth={2}
    {...props}
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      d="M4 6h16M4 12h16M4 18h16"
    />
  </svg>
)

export default Menu