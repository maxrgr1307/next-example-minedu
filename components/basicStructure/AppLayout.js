import Head from 'next/head'
import Header from './Header';
import Footer from './Footer';


import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const AppLayout = ({ title, children }) => {
    return (
        <>
            <Head>
                <title>{title ? title + ' - Amazona' : 'Amazona'}</title>
                <meta name="description" content="Ecommerce Website" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <ToastContainer position="bottom-center" limit={1} />
            <div className="flex min-h-screen flex-col justify-between">
                <Header />
                <main className="container m-auto mt-4 px-4">{children}</main>
                <Footer />
            </div>
        </>
    );
}

export default AppLayout;