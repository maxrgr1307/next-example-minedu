import Head from 'next/head'
import AppLayout from '../components/BasicStructure/AppLayout'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <>
      <AppLayout title="Home Page">
        <div className="grid grid-cols-1 gap-4 md:grid-cols-3 lg:grid-cols-4 bg-red-400">
          <h1 className='text-2xl font-bold'>
            Welcome to
          </h1>
        </div>
      </AppLayout>
    </>
  )
}
